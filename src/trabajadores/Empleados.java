/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trabajadores;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.Locale;

/**
 *
 * @author Alejandro
 */
public class Empleados {
    
    String nombre;
    String email;
    String identificador;
    BigDecimal sueldo;
    BigDecimal retencionIrpf;
    
    public Empleados(String nombre, String email, String identificador, 
            BigDecimal sueldo, BigDecimal retencionIrpf ){


    this.nombre = nombre;
    this.email  = email;
    this.identificador = identificador;
    this.retencionIrpf = retencionIrpf;
    this.sueldo = sueldo;

}
 @Override
    public String toString() {

 NumberFormat nf = NumberFormat.getCurrencyInstance(new Locale("es", "ES"));
 
    
 return "Nombre: "+ this.nombre + "\n" + "Email: "
         +this.email + "\n" + "dni: "+ this.identificador + "\n" +
          "\n"+ "Sueldo: " +nf.format(this.sueldo) + "\n" 
         +"Retencion Irpf: " + nf.format(this.retencionIrpf);
 
         
         
}    
    
    public BigDecimal cantidadAPagar(){


    return sueldo.multiply(BigDecimal.ONE.add(retencionIrpf.negate()));


}
}
